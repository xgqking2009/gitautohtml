import tkinter as tk
from tkinter import *
import json
# win.f.write("Xgq ProcessNew 2222.....:"+win.editvalue);win.f.flush();
win = tk.Tk();win.title("Auto Offical");
win.screenheight = win.winfo_screenheight();
win.process_nheight = 300;
win.process_nwidth = 200;
win.content_width = 300;
win.control_height = 100;
win.browser_width = 1400;
win.browser_x = 520;
win.process_color_up = '#ff0000'
win.process_color_down = '#00ff00'
win.content_color = '#0000ff'
win.resizable(0,0);win.geometry("+0+0");win.geometry("500x"+str(win.screenheight));
f = open('log.txt', 'w',encoding='utf-8')
win.f = f;
win.ew = 40;
win.process = json.load(open('p.json','r',encoding='utf-8'));
win.toppos = '+50+300';
win.topgeo = '400x80';
win.e = StringVar();
win.e2 = StringVar();

win.GComponents = [
	{'compType':'compPageInit','text':'打开网页'},
	{'compType':'compEdit','text':'输入框'},
	{'compType':'compButton','text':'按钮'}
];
def getComponentWindow(ctype):
	npro = win.process[win.index];nprokeys = npro.keys();
	if ctype=='compPageInit':
		top = Toplevel()
		top.geometry(win.toppos);top.geometry(win.topgeo);
		label = Label(top,text='网址：');label.place(x=10,y=20);
		if 'url' in nprokeys:
			win.e.set(win.process[win.index]['url']);
		else:
			win.e.set('请输入Url地址');
		edit = Entry(top,width=win.ew,textvariable=win.e);edit.place(x=80,y=20);
		win.editvalue = edit.get();
		def DestoryTop():
			win.editvalue = edit.get();top.destroy();
		top.protocol("WM_DELETE_WINDOW",DestoryTop)
		return top;
	elif ctype=='compEdit':
		top = Toplevel()
		top.geometry(win.toppos);top.geometry(win.topgeo);
		label = Label(top,text='XPath：');label.place(x=10,y=20);
		if 'xpath' in nprokeys:
			win.e.set(win.process[win.index]['xpath']);
		else:
			win.e.set('请输入XPath路径');
		edit = Entry(top,width=win.ew,textvariable=win.e);edit.place(x=80,y=20);
		win.editvalue = edit.get();
		label2 = Label(top,text='内容：');label2.place(x=10,y=40);
		if 'value' in nprokeys:
			win.e2.set(win.process[win.index]['value']);
		else:
			win.e2.set('请输入编辑内容');
		edit2 = Entry(top,width=win.ew,textvariable=win.e2);edit2.place(x=80,y=40);
		win.editvalue2 = edit2.get();
		def DestoryTop():
			win.editvalue = edit.get();win.editvalue2 = edit2.get();top.destroy();
		top.protocol("WM_DELETE_WINDOW",DestoryTop)
		return top;
	elif ctype=='compButton':
		top = Toplevel()
		top.geometry(win.toppos);top.geometry(win.topgeo);
		label = Label(top,text='网址：');label.place(x=10,y=20);
		if 'xpath' in nprokeys:
			win.e.set(win.process[win.index]['xpath']);
		else:
			win.e.set('请输入XPath路径');
		edit = Entry(top,width=win.ew,textvariable=win.e);edit.place(x=80,y=20);
		win.editvalue = edit.get();
		def DestoryTop():
			win.editvalue = edit.get();top.destroy();
		top.protocol("WM_DELETE_WINDOW",DestoryTop)
		return top;

def getElementByXPath(driver,xpath):
	context_pos = xpath.find('<context>');
	if context_pos>=0:
		subxpath = xpath[0:context_pos];
		conpath = xpath[context_pos:len(xpath)];
		endcon = conpath.find('</context>');
		contextpath = conpath[9:endcon];
		driver.switch_to.default_content();
		context = driver.find_element_by_xpath(contextpath);
		driver.switch_to.frame(context);
		element = driver.find_element_by_xpath(subxpath);
		return element;
	else:
		driver.switch_to.default_content();
		element = driver.find_element_by_xpath(xpath);
		return element;
def log(str):
	win.f.write(str+"\n");win.f.flush();
win.getComponentWindow = getComponentWindow;
win.getElementByXPath = getElementByXPath;
win.log = log;