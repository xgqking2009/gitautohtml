from tkinter import *
from win import win
import json;
from frame_process import listbox_process_list
frame_content = Frame(height=win.process_nheight*3, width=win.content_width,bg=win.content_color)
frame_content.grid(row=0, column=1,rowspan=2)

listbox_content = Listbox(frame_content)
win.listbox_content = listbox_content;
def addComponent(v):
	if isinstance(v,int):
		win.listbox_content.insert(END,win.listbox_component.get(v))
		win.process.append({'compType':win.GComponents[v]['compType']})
	else:
		for comp in win.GComponents:
			if comp['compType'] == v:
				win.listbox_content.insert(END,comp['text'])
def editComponent(event):
	index = win.listbox_content.nearest(event.y)
	win.index = index;
	top = win.getComponentWindow(win.process[index]['compType']);
	win.wait_window(top)
	compType = win.process[index]['compType'];
	if compType == 'compPageInit':
		win.process[index]['url'] = win.editvalue;
	elif compType == 'compEdit':
		win.process[index]['xpath'] = win.editvalue;
		win.process[index]['value'] = win.editvalue2;
	elif compType == 'compButton':
		win.process[index]['xpath'] = win.editvalue;
	f = open('p.json', 'w',encoding='utf-8')
	f.write(json.dumps(win.process,ensure_ascii=False))
	f.close()

win.addComponent = addComponent;
listbox_content.bind("<Double-Button-1>",editComponent)
listbox_content.place(bordermode=INSIDE, height=win.process_nheight*3, width=win.content_width)