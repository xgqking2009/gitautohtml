from tkinter import *
from win import win
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os
import time

frame_process_list = Frame(height = win.process_nheight, width=win.process_nwidth,bg= win.process_color_up)
frame_component = Frame(height = win.process_nheight*2, width=win.process_nwidth,bg= win.process_color_down)
frame_control = Frame(height = win.control_height, width=win.process_nwidth,bg= win.process_color_down)
frame_process_list.grid(row=0, column=0)
frame_component.grid(row=1, column=0)
frame_control.place(x=0,y=win.process_nheight*3,width = win.process_nwidth+win.content_width)

def clickPlay():
	executable_path = "../chromedriver_win32/chromedriver.exe"
	os.environ["webdriver.chrome.driver"] = executable_path
	driver = webdriver.Chrome(executable_path=executable_path)
	driver.set_window_position(win.browser_x,0)
	driver.set_window_size(win.browser_width,win.screenheight)
	for npro in win.process:
		if npro['compType'] == 'compPageInit':
			driver.get(npro['url'])
		elif npro['compType'] == 'compEdit':
			edit = win.getElementByXPath(driver,npro['xpath'])
			edit.send_keys(npro['value'])
		elif npro['compType'] == 'compButton':
			button = win.getElementByXPath(driver,npro['xpath'])
			time.sleep(1)
			try:
				button.send_keys(Keys.ENTER)
			except Exception as e:
				button.click()
			else:
				pass
			finally:
				pass
	return;
	
frame_control_button_play = Button(frame_control,text="播放",width=40,command = clickPlay)
frame_control_button_play.place(x=100,y=20)
listbox_process_list = Listbox(frame_process_list)
listbox_process_list.insert(END,'测试流程')
# listbox_process_list.pack()
listbox_process_list.place(bordermode=INSIDE, height=win.process_nheight, width=win.process_nwidth)
# frame_process_button.
listbox_component = Listbox(frame_component)

for comp in win.GComponents:
	listbox_component.insert(END,comp['text'])
# print("listbox_compoent[0]:"+listbox_component.get(0))
listbox_component_drag_index = 0
def setCurrent(event):
	global listbox_component_drag_index;
	listbox_component_drag_index = listbox_component.nearest(event.y)
	win.addComponent(listbox_component_drag_index)
listbox_component.bind("<Double-Button-1>",setCurrent)
listbox_component.place(bordermode=INSIDE, height=win.process_nheight*2, width=win.process_nwidth)
print("Xgq set win.listbox_component");
win.listbox_component = listbox_component;

